--category table initial data--
INSERT INTO category (description) VALUES ('American')
INSERT INTO category (description) VALUES ('Italian')
INSERT INTO category (description) VALUES ('Mexican')
INSERT INTO category (description) VALUES ('Fast Food')
--unit of measure table initial data--
INSERT INTO unit_of_measure (description) VALUES ('Teaspoon')
INSERT INTO unit_of_measure (description) VALUES ('Tablespoon')
INSERT INTO unit_of_measure (description) VALUES ('Cup')
--from one pot red wine pasta--
INSERT INTO unit_of_measure (description) VALUES ('Oz.')
INSERT INTO unit_of_measure (description) VALUES ('Small')
INSERT INTO unit_of_measure (description) VALUES ('Clove')
INSERT INTO unit_of_measure (description) VALUES ('Sprig')
INSERT INTO unit_of_measure (description) VALUES ('Leaf')
INSERT INTO unit_of_measure (description) VALUES ('Some')