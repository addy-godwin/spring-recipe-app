package com.orbilax.springrecipe.controllers

import com.orbilax.springrecipe.services.ImageService
import com.orbilax.springrecipe.services.RecipeService
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import javax.servlet.http.HttpServletResponse

@Controller
class ImageController(private val imageService: ImageService,
                      private val recipeService: RecipeService) {

    @GetMapping("recipe/{recipeId}/image")
    fun showUploadForm(@PathVariable recipeId: Long, model: Model): String {
        model["recipe"] = recipeService.findCommandById(recipeId)

        return "recipe/imageuploadform"
    }

    @PostMapping("/recipe/{id}/image")
    fun handleImagePost(@PathVariable id: Long,
                        @RequestParam("imagefile") file: MultipartFile): String {
        imageService.saveImageFile(id, file)

        return "redirect:/recipe/$id/show"
    }

    @GetMapping("/recipe/{id}/recipeimage")
    fun renderFromDB(@PathVariable id: Long, response: HttpServletResponse) {
        val recipeCommand = recipeService.findCommandById(id)

        if(recipeCommand.image == null) return

        response.contentType = "image/jpeg"
        val inputStream = ByteArrayInputStream(recipeCommand.image)
        IOUtils.copy(inputStream, response.outputStream)
    }
}