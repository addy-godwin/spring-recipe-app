package com.orbilax.springrecipe.controllers

import com.orbilax.springrecipe.exception.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.ModelAndView

@ControllerAdvice
class ControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NumberFormatException::class)
    fun handleNumberFormatException(exception: Exception): ModelAndView {
        val modelAndView = ModelAndView()
        modelAndView.viewName = "400error"
        modelAndView.addObject("exception", exception)
        return modelAndView
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException::class)
    fun handleNotFound(exception: Exception): ModelAndView {
        val modelAndView = ModelAndView()
        modelAndView.viewName = "404error"
        modelAndView.addObject("exception", exception)
        return modelAndView
    }
}