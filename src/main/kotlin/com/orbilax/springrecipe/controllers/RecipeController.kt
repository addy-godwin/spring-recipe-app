package com.orbilax.springrecipe.controllers

import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.services.RecipeService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import javax.validation.Valid

@Controller
class RecipeController(private val recipeService: RecipeService) {

    @GetMapping("/recipe/all")
    fun getRecipes(model: Model): String {
        model["recipes"] = recipeService.getRecipes()
        return "recipe/all"
    }

    @GetMapping("recipe/{id}/show")
    fun getRecipeById(@PathVariable id: Long, model: Model): String {
        model["recipe"] = recipeService.findRecipeById(id)
        return "recipe/show"
    }

    @GetMapping("recipe/new")
    fun submitRecipe(model: Model): String {
        model["recipe"] = RecipeCommand()
        return RECIPE_FORM_URL
    }

    @GetMapping("recipe/{id}/update")
    fun updateRecipe(@PathVariable id: Long, model: Model): String {
        model["recipe"] = recipeService.findCommandById(id)
        return RECIPE_FORM_URL
    }

    @PostMapping("recipe/save")
    fun saveOrUpdateRecipe(@Valid @ModelAttribute("recipe") recipeCommand: RecipeCommand,
                           bindingResult: BindingResult): String {
        if(bindingResult.hasErrors()){
            bindingResult.allErrors.forEach { println(it) }
            return RECIPE_FORM_URL
        }
        val savedRecipeCommand = recipeService.saveRecipeCommand(recipeCommand)
        return "redirect:/recipe/${savedRecipeCommand.id}/show"
    }

    @GetMapping("recipe/{id}/delete")
    fun deleteRecipe(@PathVariable id: Long): String {
        recipeService.deleteById(id)
        return "redirect:/recipe/all"
    }

    companion object {
        private const val   RECIPE_FORM_URL = "recipe/recipeform"
    }

}