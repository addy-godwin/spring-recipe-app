package com.orbilax.springrecipe.controllers

import com.orbilax.springrecipe.commands.IngredientCommand
import com.orbilax.springrecipe.commands.UnitOfMeasureCommand
import com.orbilax.springrecipe.services.IngredientService
import com.orbilax.springrecipe.services.RecipeService
import com.orbilax.springrecipe.services.UnitOfMeasureService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping

@Controller
class IngredientController(private val recipeService: RecipeService,
                           private val ingredientService: IngredientService,
                           private val unitOfMeasureService: UnitOfMeasureService) {


    @GetMapping("/recipe/{id}/ingredients")
    fun listIngredients(@PathVariable id: Long, model: Model): String {
        model["recipe"] = recipeService.findCommandById(id)

        return "recipe/ingredient/list"
    }

    @GetMapping("/recipe/{recipeId}/ingredient/{id}/show")
    fun showIngredient(@PathVariable recipeId: Long,
                       @PathVariable id: Long, model: Model): String {

        model["ingredient"] = ingredientService.findByRecipeIdAndIngredientId(recipeId, id)

        return "recipe/ingredient/show"
    }

    @GetMapping("/recipe/{recipeId}/ingredient/new")
    fun newRecipe(@PathVariable recipeId: Long, model: Model): String {

        val recipeCommand = recipeService.findCommandById(recipeId)
        //todo raise exception

        val ingredientCommand =  IngredientCommand()
        ingredientCommand.recipeId = recipeId
        model["ingredient"] = ingredientCommand

        ingredientCommand.unitOfMeasure = UnitOfMeasureCommand()

        model["uomList"] = unitOfMeasureService.listAllUoms() //init uom

        return "recipe/ingredient/ingredientform"
    }

    @GetMapping("/recipe/{recipeId}/ingredient/{id}/update")
    fun updateRecipeIngredient(@PathVariable recipeId: Long,
                               @PathVariable id: Long, model: Model): String {

        model["ingredient"] = ingredientService.findByRecipeIdAndIngredientId(recipeId, id)

        model["uomList"] = unitOfMeasureService.listAllUoms()

        return "recipe/ingredient/ingredientform"
    }

    @PostMapping("/recipe/{recipeId}/ingredient")
    fun saveOrUpdate(@ModelAttribute ingredientCommand: IngredientCommand): String {

        val savedCommand = ingredientService.saveIngredientCommand(ingredientCommand)

        return "redirect:/recipe/" + savedCommand.recipeId + "/ingredient/" + savedCommand.id + "/show"
    }

    @GetMapping("/recipe/{recipeId}/ingredient/{ingredientId}/delete")
    fun deleteIngredient(@PathVariable recipeId: Long,
                         @PathVariable ingredientId: Long,
                         model: Model): String {

        ingredientService.deleteById(recipeId, ingredientId)

        return "redirect:/recipe/$recipeId/ingredients"
    }

}