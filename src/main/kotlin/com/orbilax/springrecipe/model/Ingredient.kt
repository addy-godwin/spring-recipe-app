package com.orbilax.springrecipe.model

import java.math.BigDecimal
import javax.persistence.*

@Entity
class Ingredient() {

    constructor(description: String, amount: BigDecimal, uom: UnitOfMeasure) : this() {
        this.description = description
        this.amount = amount
        this.unitOfMeasure = uom
        this.recipe = recipe
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    var description: String = ""
    var amount: BigDecimal? = null

    @OneToOne(fetch = FetchType.EAGER)
    var unitOfMeasure: UnitOfMeasure? = null

    @ManyToOne
    var recipe: Recipe? = null
}