package com.orbilax.springrecipe.model

enum class Difficulty {
    EASY,
    MODERATE,
    HARD
}