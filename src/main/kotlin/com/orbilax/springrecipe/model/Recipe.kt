package com.orbilax.springrecipe.model

import javax.persistence.*

@Entity
class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    var description: String = ""
    var prepTime: Int? = 0
    var cookTime: Int? = 0
    var servings: Int? = 0
    var source: String = ""
    var url: String = ""
    var submittedBy: String = ""

    @Lob
    var directions: String = ""

    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "recipe")
    var ingredients: Set<Ingredient> = HashSet()

    @Lob
    var image: ByteArray? = null

    @Enumerated(value = EnumType.STRING)
    var difficulty: Difficulty? = null

    @ManyToMany
    @JoinTable(name = "recipe_category",
            joinColumns = [(JoinColumn(name = "recipe_id"))],
            inverseJoinColumns = [(JoinColumn(name = "category_id"))])
    var categories: Set<Category> = HashSet()

    @OneToOne(cascade = [CascadeType.ALL])
    var notes: Notes? = null
        set(value) {
            field = value
            field?.recipe = this
        }

    fun addCategory(category: Category){
        (categories as MutableSet).add(category)
    }

    fun addIngredient(ingredient: Ingredient){
        (ingredients as MutableSet).add(ingredient)
        ingredient.recipe = this
    }

    fun removeIngredient(ingredient: Ingredient) {
        (ingredients as MutableSet).remove(ingredient)
    }
}