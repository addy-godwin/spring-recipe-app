package com.orbilax.springrecipe

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringrecipeApplication

fun main(args: Array<String>) {
    runApplication<SpringrecipeApplication>(*args)
}