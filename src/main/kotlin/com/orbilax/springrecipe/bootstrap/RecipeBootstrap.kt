package com.orbilax.springrecipe.bootstrap

import com.orbilax.springrecipe.model.Difficulty
import com.orbilax.springrecipe.model.Ingredient
import com.orbilax.springrecipe.model.Notes
import com.orbilax.springrecipe.model.Recipe
import com.orbilax.springrecipe.repository.CategoryRepository
import com.orbilax.springrecipe.repository.RecipeRepository
import com.orbilax.springrecipe.repository.UnitOfMeasureRepository
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.util.*

@Component
class RecipeBootstrap(private val recipeRepository: RecipeRepository,
                      private val categoryRepo: CategoryRepository,
                      private val unitOfMeasureRepo: UnitOfMeasureRepository): ApplicationListener<ContextRefreshedEvent>{

    @Transactional
    override fun onApplicationEvent(p0: ContextRefreshedEvent) {
        recipeRepository.saveAll(getRecipes())
    }

    private fun getRecipes(): Set<Recipe> {
        val recicpes = HashSet<Recipe>()

        val onePotRedWinePasta = Recipe()

        onePotRedWinePasta.description = "One-Pot Red Wine Pasta"
        onePotRedWinePasta.cookTime = 20
        onePotRedWinePasta.prepTime = 10
        onePotRedWinePasta.servings = 5
        onePotRedWinePasta.source = "Food Lion"
        onePotRedWinePasta.url = "https://www.foodlion.com/recipes/one-pot-red-wine-pasta/"
        onePotRedWinePasta.submittedBy = "Ménage à Trois"
        onePotRedWinePasta.difficulty = Difficulty.EASY

        onePotRedWinePasta.directions = """
            Finely cut the onion and mince the garlic. Add all ingredients (except the Parmesan cheese) to a large pot.
            Bring to a boil. Reduce the heat to a low boil and cook for 7-8 minutes, stirring occasionally.
            Once the noodles are cooked through, remove the bay leaf and basil sprigs before serving.
            Top with a little more pepper and basil, then add the cheese. Serves 4-5.
        """.trimIndent()

        val onePotRedWinePastaNote = Notes()
        onePotRedWinePastaNote.recipeNotes = """
             The notoriously romantic Italian language is here to tantalize our taste buds with a dish that lingers on the tongue: Spaghetti All’Ubriaco.
             While the English translation (drunken spaghetti) is not quite as eloquent, we assure you, this dish tastes as good as its given name sounds.
             Cooked in Ménage à Trois Lavish Merlot instead of water, this one-pot meal is easy to prepare—and even easier to clean up. Now that’s amore.
        """.trimIndent()
        onePotRedWinePasta.notes = onePotRedWinePastaNote

        onePotRedWinePasta.addCategory(categoryRepo.findByDescription("Italian").get())

        onePotRedWinePasta.addIngredient(Ingredient("linguine", BigDecimal(16), unitOfMeasureRepo.findByDescription("Oz.").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("white onion", BigDecimal(1), unitOfMeasureRepo.findByDescription("Small").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("garlic", BigDecimal(2), unitOfMeasureRepo.findByDescription("Clove").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("fresh basil", BigDecimal(2), unitOfMeasureRepo.findByDescription("Sprig").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("bay", BigDecimal(1), unitOfMeasureRepo.findByDescription("Leaf").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("olive oil", BigDecimal(2), unitOfMeasureRepo.findByDescription("Tablespoon").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("Ménage à Trois Lavish Merlot", BigDecimal(2), unitOfMeasureRepo.findByDescription("Cup").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("water", BigDecimal(0.75), unitOfMeasureRepo.findByDescription("Cup").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("salt and pepper", BigDecimal(0), unitOfMeasureRepo.findByDescription("Some").get()) )
        onePotRedWinePasta.addIngredient(Ingredient("Parmesan cheese to serve", BigDecimal(0.5), unitOfMeasureRepo.findByDescription("Cup").get()) )

        recicpes.add(onePotRedWinePasta)
        return recicpes
    }
}