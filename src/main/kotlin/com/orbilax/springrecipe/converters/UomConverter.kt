package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.commands.UnitOfMeasureCommand
import com.orbilax.springrecipe.model.UnitOfMeasure
import org.jetbrains.annotations.Nullable
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class UomCommandToUom: Converter<UnitOfMeasureCommand, UnitOfMeasure>{

    @Synchronized
    override fun convert(@Nullable source: UnitOfMeasureCommand): UnitOfMeasure {
        val unitOfMeasure = UnitOfMeasure()
        unitOfMeasure.id = source.id
        unitOfMeasure.description = source.description
        return unitOfMeasure
    }
}

@Component
class UomToUomCommand: Converter<UnitOfMeasure, UnitOfMeasureCommand>{

    @Synchronized
    override fun convert(source: UnitOfMeasure): UnitOfMeasureCommand {
        val unitOfMeasureCommand = UnitOfMeasureCommand()
        unitOfMeasureCommand.id = source.id
        unitOfMeasureCommand.description = source.description
        return unitOfMeasureCommand
    }
}