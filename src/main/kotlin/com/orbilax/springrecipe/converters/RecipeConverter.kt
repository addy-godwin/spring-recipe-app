package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.model.Recipe
import org.springframework.core.convert.converter.Converter
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component

@Component
class RecipeCommandToRecipe(private val categoryConverter: CategoryCommandToCategory,
                            private val ingredientConverter: IngredientCommandToIngredient,
                            private val notesConverter: NotesCommandToNotes) : Converter<RecipeCommand, Recipe> {

    @Synchronized
    override fun convert(source: RecipeCommand): Recipe {

        val recipe = Recipe()
        recipe.id = source.id
        recipe.cookTime = source.cookTime
        recipe.prepTime = source.prepTime
        recipe.description = source.description
        recipe.difficulty = source.difficulty
        recipe.directions = source.directions
        recipe.servings = source.servings
        recipe.source = source.source
        recipe.url = source.url
        recipe.submittedBy = source.submittedBy
        recipe.notes = notesConverter.convert(source.notes)

        if (source.categories.isNotEmpty()) {
            source.categories.forEach { category ->
                recipe.addCategory(categoryConverter.convert(category))
            }
        }

        if (source.ingredients.isNotEmpty()) {
            source.ingredients.forEach { ingredient ->
                recipe.addIngredient(ingredientConverter.convert(ingredient))
            }
        }

        return recipe
    }
}


@Component
class RecipeToRecipeCommand(private val categoryConverter: CategoryToCategoryCommand,
                            private val ingredientConverter: IngredientToIngredientCommand,
                            private val notesConverter: NotesToNotesCommand) : Converter<Recipe, RecipeCommand> {

    @Synchronized
    @Nullable
    override fun convert(source: Recipe): RecipeCommand {

        val recipe = RecipeCommand()
        recipe.id = source.id
        recipe.cookTime = source.cookTime
        recipe.prepTime = source.prepTime
        recipe.description = source.description
        recipe.difficulty = source.difficulty
        recipe.directions = source.directions
        recipe.servings = source.servings
        recipe.source = source.source
        recipe.url = source.url
        recipe.image = source.image
        recipe.submittedBy = source.submittedBy
        recipe.notes = notesConverter.convert(source.notes!!)

        if (source.categories.isNotEmpty()) {
            source.categories.forEach { category ->
                recipe.categories.add(categoryConverter.convert(category))
            }
        }

        if (source.ingredients.isNotEmpty()) {
            source.ingredients.forEach { ingredient ->
                recipe.ingredients.add(ingredientConverter.convert(ingredient))
            }
        }

        return recipe
    }
}