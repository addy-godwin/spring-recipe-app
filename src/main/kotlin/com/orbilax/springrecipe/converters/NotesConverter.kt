package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.commands.NotesCommand
import com.orbilax.springrecipe.model.Notes
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class NotesCommandToNotes: Converter<NotesCommand, Notes>{

    @Synchronized
    override fun convert(source: NotesCommand): Notes {
        val notes = Notes()
        notes.id = source.id
        notes.recipeNotes = source.recipeNotes
        return notes
    }
}

@Component
class NotesToNotesCommand: Converter<Notes, NotesCommand>{

    @Synchronized
    override fun convert(source: Notes): NotesCommand {
        val notesCommand = NotesCommand()
        notesCommand.id = source.id
        notesCommand.recipeNotes = source.recipeNotes
        return notesCommand
    }
}