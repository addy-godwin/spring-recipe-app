package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.commands.CategoryCommand
import com.orbilax.springrecipe.model.Category
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class CategoryCommandToCategory: Converter<CategoryCommand, Category>{

    @Synchronized
    override fun convert(source: CategoryCommand): Category {

       val category = Category()
        category.id  = source.id
        category.description = source.description
        return category

    }
}

@Component
class CategoryToCategoryCommand: Converter<Category, CategoryCommand>{

    @Synchronized
    override fun convert(source: Category): CategoryCommand {

       val categoryCommand = CategoryCommand()
        categoryCommand.id  = source.id
        categoryCommand.description = source.description
        return categoryCommand

    }
}