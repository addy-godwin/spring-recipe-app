package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.commands.IngredientCommand
import com.orbilax.springrecipe.model.Ingredient
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class IngredientCommandToIngredient(private val uomConverter: UomCommandToUom): Converter<IngredientCommand, Ingredient>{

    @Synchronized
    override fun convert(source: IngredientCommand): Ingredient {
        val ingredient  = Ingredient()
        ingredient.id = source.id
        ingredient.amount = source.amount
        ingredient.description = source.description

        if (source.unitOfMeasure != null) ingredient.unitOfMeasure = uomConverter.convert(source.unitOfMeasure!!)

        return ingredient
    }

}

@Component
class IngredientToIngredientCommand(private val uomConverter: UomToUomCommand): Converter<Ingredient, IngredientCommand>{

    @Synchronized
    override fun convert(source: Ingredient): IngredientCommand {
        val ingredientCommand  = IngredientCommand()
        ingredientCommand.id = source.id

        if(source.recipe != null) ingredientCommand.recipeId = source.recipe!!.id

        ingredientCommand.amount = source.amount
        ingredientCommand.description = source.description

        if (source.unitOfMeasure != null) ingredientCommand.unitOfMeasure = uomConverter.convert(source.unitOfMeasure!!)

        return ingredientCommand
    }

}