package com.orbilax.springrecipe.repository

import com.orbilax.springrecipe.model.Recipe
import org.springframework.data.repository.CrudRepository

interface RecipeRepository : CrudRepository<Recipe, Long>