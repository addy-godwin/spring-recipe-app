package com.orbilax.springrecipe.repository

import com.orbilax.springrecipe.model.UnitOfMeasure
import org.springframework.data.repository.CrudRepository
import java.util.*

interface UnitOfMeasureRepository : CrudRepository<UnitOfMeasure, Long> {

    fun findByDescription(description: String): Optional<UnitOfMeasure>

}