package com.orbilax.springrecipe.repository

import com.orbilax.springrecipe.model.Category
import org.springframework.data.repository.CrudRepository
import java.util.*

interface CategoryRepository : CrudRepository<Category, Long>{

    fun findByDescription(description: String): Optional<Category>
}