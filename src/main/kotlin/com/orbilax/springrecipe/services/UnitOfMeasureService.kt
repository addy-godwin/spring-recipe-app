package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.commands.UnitOfMeasureCommand

interface UnitOfMeasureService {

    fun listAllUoms(): Set<UnitOfMeasureCommand>

}