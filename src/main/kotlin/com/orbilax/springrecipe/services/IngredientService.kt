package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.commands.IngredientCommand

interface IngredientService {

    fun findByRecipeIdAndIngredientId(recipeId: Long, ingredientId: Long): IngredientCommand

    fun saveIngredientCommand(ingredientCommand: IngredientCommand): IngredientCommand

    fun deleteById(recipeId: Long, ingredientId: Long)
}