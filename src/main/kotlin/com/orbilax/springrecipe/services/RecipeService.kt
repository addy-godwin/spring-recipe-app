package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.model.Category
import com.orbilax.springrecipe.model.Recipe

interface RecipeService {
    fun getRecipes(): Set<Recipe>

    fun getRecipeCategories(): Set<Category>

    fun findRecipeById(id: Long): Recipe

    fun findCommandById(id: Long): RecipeCommand

    fun saveRecipeCommand(command: RecipeCommand):  RecipeCommand

    fun deleteById(idToDelete: Long)
}