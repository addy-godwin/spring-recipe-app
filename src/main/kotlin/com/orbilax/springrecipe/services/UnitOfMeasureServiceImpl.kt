package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.commands.UnitOfMeasureCommand
import com.orbilax.springrecipe.converters.UomToUomCommand
import com.orbilax.springrecipe.repository.UnitOfMeasureRepository
import org.springframework.stereotype.Service
import java.util.stream.Collectors
import java.util.stream.StreamSupport

@Service
class UnitOfMeasureServiceImpl(private val unitOfMeasureRepository: UnitOfMeasureRepository,
                               private val uomToUomCommand: UomToUomCommand) : UnitOfMeasureService {

    override fun listAllUoms(): Set<UnitOfMeasureCommand> {
        return StreamSupport.stream(unitOfMeasureRepository.findAll()
                .spliterator(), false)
                .map { uomToUomCommand.convert(it) }
                .collect(Collectors.toSet())
    }
}