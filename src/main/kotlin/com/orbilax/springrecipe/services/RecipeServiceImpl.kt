package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.converters.RecipeCommandToRecipe
import com.orbilax.springrecipe.converters.RecipeToRecipeCommand
import com.orbilax.springrecipe.exception.NotFoundException
import com.orbilax.springrecipe.model.Category
import com.orbilax.springrecipe.model.Recipe
import com.orbilax.springrecipe.repository.CategoryRepository
import com.orbilax.springrecipe.repository.RecipeRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class RecipeServiceImpl(private val recipeRepository: RecipeRepository,
                        private val categoryRepository: CategoryRepository,
                        private val recipeCommandToRecipe: RecipeCommandToRecipe,
                        private val recipeToRecipeCommand: RecipeToRecipeCommand): RecipeService {

    override fun getRecipes(): Set<Recipe> {
        val allRecipes = HashSet<Recipe>()

        recipeRepository.findAll().forEach { allRecipes.add(it) }
        return allRecipes
    }

    override fun findRecipeById(id: Long): Recipe {
        val recipeOptional = recipeRepository.findById(id)

        if (!recipeOptional.isPresent) throw NotFoundException("Recipe Not Found. For id value $id")

        return  recipeOptional.get()
    }

    override fun getRecipeCategories(): Set<Category> {
        val allCategories = HashSet<Category>()
        categoryRepository.findAll().forEach { allCategories.add(it) }
        return allCategories
    }

    @Transactional
    override fun saveRecipeCommand(command: RecipeCommand): RecipeCommand {
        val detachedRecipe = recipeCommandToRecipe.convert(command)
        val savedRecipe = recipeRepository.save(detachedRecipe)
        return recipeToRecipeCommand.convert(savedRecipe)
    }

    @Transactional
    override fun findCommandById(id: Long): RecipeCommand {
        return recipeToRecipeCommand.convert(findRecipeById(id))
    }

    override fun deleteById(idToDelete: Long) {
        recipeRepository.deleteById(idToDelete)
    }
}