package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.commands.IngredientCommand
import com.orbilax.springrecipe.converters.IngredientCommandToIngredient
import com.orbilax.springrecipe.converters.IngredientToIngredientCommand
import com.orbilax.springrecipe.repository.RecipeRepository
import com.orbilax.springrecipe.repository.UnitOfMeasureRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class IngredientServiceImpl(private val recipeRepository: RecipeRepository,
                            private val unitOfMeasureRepository: UnitOfMeasureRepository,
                            private val ingredientToIngredientCommand: IngredientToIngredientCommand,
                            private val ingredientCommandToIngredient: IngredientCommandToIngredient) : IngredientService {

    override fun findByRecipeIdAndIngredientId(recipeId: Long, ingredientId: Long): IngredientCommand {

        val recipeOptional = recipeRepository.findById(recipeId)

        val recipe = recipeOptional.get()

        val ingredientCommandOptional = recipe.ingredients.stream()
                .filter { it.id == ingredientId }
                .map { ingredientToIngredientCommand.convert(it) }
                .findFirst()

        return ingredientCommandOptional.get()
    }

    @Transactional
    override fun saveIngredientCommand(ingredientCommand: IngredientCommand): IngredientCommand {

        val recipeOptional = recipeRepository.findById(ingredientCommand.recipeId)

        if (!recipeOptional.isPresent) return IngredientCommand() //todo

        val recipe = recipeOptional.get()

        val ingredientOptional = recipe
                .ingredients
                .stream()
                .filter { ingredient -> ingredient.id == ingredientCommand.id }
                .findFirst()

        if (ingredientOptional.isPresent) {

            val ingredientFound = ingredientOptional.get()
            ingredientFound.description = ingredientCommand.description
            ingredientFound.amount = ingredientCommand.amount

            ingredientFound.unitOfMeasure = unitOfMeasureRepository
                    .findById(ingredientCommand.unitOfMeasure!!.id)
                    .orElseThrow { RuntimeException("UOM NOT FOUND") } //todo address this

        } else {
            //add new ingredient
            val ingredient = ingredientCommandToIngredient.convert(ingredientCommand)
            ingredient.recipe = recipe
            recipe.addIngredient(ingredient)
        }

        val savedRecipe = recipeRepository.save(recipe)

        var savedIngredientOptional = savedRecipe.ingredients.stream()
                .filter{ it.id == ingredientCommand.id}
                .findFirst()

        if(!savedIngredientOptional.isPresent) {

            savedIngredientOptional = savedRecipe.ingredients.stream()
                    .filter { it.amount == ingredientCommand.amount }
                    .filter { it.description == ingredientCommand.description }
                    .filter { it.unitOfMeasure?.id == ingredientCommand.unitOfMeasure?.id }
                    .findFirst()
        }

        //todo check for fail

        return ingredientToIngredientCommand.convert(savedIngredientOptional.get())
    }


    @Transactional
    override fun deleteById(recipeId: Long, ingredientId: Long) {

        val recipeOptional = recipeRepository.findById(recipeId)

        if (!recipeOptional.isPresent) return //todo handle error of deleting ingredient on non-existent recipe

        val deletedIngredient = recipeOptional.get().ingredients.stream()
                .filter { it.id == ingredientId }
                .findFirst()

        if(!deletedIngredient.isPresent) return

        deletedIngredient.get().recipe = null
        recipeOptional.get().removeIngredient(deletedIngredient.get())

        recipeRepository.save(recipeOptional.get())
    }
}