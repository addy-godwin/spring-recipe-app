package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.repository.RecipeRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.io.IOException

@Service
class ImageServiceImpl(private val recipeRepository: RecipeRepository): ImageService {

    @Transactional
    override fun saveImageFile(recipeId: Long, file: MultipartFile) {

        try {
            val recipe = recipeRepository.findById(recipeId).get()

            recipe.image = file.bytes.copyOf()
            recipeRepository.save(recipe)

        }catch (e: IOException) {
            //todo handle better
            println("Error occurred")
            e.printStackTrace()
        }
    }
}