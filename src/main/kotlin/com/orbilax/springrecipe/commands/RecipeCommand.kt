package com.orbilax.springrecipe.commands

import com.orbilax.springrecipe.model.Difficulty
import org.hibernate.validator.constraints.URL
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class RecipeCommand {

    var id: Long = 0

    @NotBlank
    @Size(min = 3, max = 255)
    var description: String = ""

    @Min(1)
    @Max(999)
    var prepTime: Int? = null

    @Min(1)
    @Max(999)
    var cookTime: Int? = null

    @Min(1)
    @Max(100)
    var servings: Int? = null
    var source: String = ""

    @URL
    var url: String = ""

    @NotBlank
    var directions: String = ""
    var submittedBy: String = ""
    var ingredients: MutableSet<IngredientCommand> = HashSet()
    var difficulty: Difficulty? = null
    var notes: NotesCommand = NotesCommand()
    var categories: MutableSet<CategoryCommand> = HashSet()
    var image: ByteArray? = null
}