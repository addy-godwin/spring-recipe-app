package com.orbilax.springrecipe.commands

class UnitOfMeasureCommand {
    var id: Long = 0
    var description: String = ""
}