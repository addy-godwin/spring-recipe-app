package com.orbilax.springrecipe.commands

class NotesCommand {
    var id: Long = 0
    var recipeNotes: String = ""
}