package com.orbilax.springrecipe.commands

class CategoryCommand {
    var id: Long = 0
    var description: String = ""
}