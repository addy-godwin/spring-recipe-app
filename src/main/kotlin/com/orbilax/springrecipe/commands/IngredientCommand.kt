package com.orbilax.springrecipe.commands

import java.math.BigDecimal

class IngredientCommand {
    var id: Long = 0
    var recipeId: Long = 0
    var description: String = ""
    var amount: BigDecimal? = null
    var unitOfMeasure: UnitOfMeasureCommand? = null
}