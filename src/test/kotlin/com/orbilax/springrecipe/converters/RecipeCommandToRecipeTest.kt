package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.commands.CategoryCommand
import com.orbilax.springrecipe.commands.IngredientCommand
import com.orbilax.springrecipe.commands.NotesCommand
import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.model.Difficulty
import org.junit.Assert.assertEquals
import org.junit.Test

class RecipeCommandToRecipeTest {

    private val converter = RecipeCommandToRecipe(CategoryCommandToCategory(),
            IngredientCommandToIngredient(UomCommandToUom()),
            NotesCommandToNotes())

    @Test
    fun convert() {
        //given
        val recipeCommand = RecipeCommand()
        recipeCommand.id = RECIPE_ID
        recipeCommand.cookTime = COOK_TIME
        recipeCommand.prepTime = PREP_TIME
        recipeCommand.description = DESCRIPTION
        recipeCommand.directions = DIRECTIONS
        recipeCommand.servings = SERVINGS
        recipeCommand.source = SOURCE
        recipeCommand.url = URL
        recipeCommand.submittedBy = SUBMITTED_BY
        recipeCommand.difficulty = DIFFICULTY

        val notesCommand = NotesCommand()
        notesCommand.id = NOTES_ID
        recipeCommand.notes = notesCommand

        val categoryCommand = CategoryCommand()
        categoryCommand.id = CAT_ID1
        val categoryCommand2 = CategoryCommand()
        categoryCommand2.id = CAT_ID2

        recipeCommand.categories.add(categoryCommand)
        recipeCommand.categories.add(categoryCommand2)

        val ingredientCommand = IngredientCommand()
        ingredientCommand.id = INGRED_ID1
        val ingredientCommand2 = IngredientCommand()
        ingredientCommand2.id = INGRED_ID2

        recipeCommand.ingredients.add(ingredientCommand)
        recipeCommand.ingredients.add(ingredientCommand2)

        //when
        val recipe = converter.convert(recipeCommand)

        //then
        assertEquals(RECIPE_ID, recipe.id)
        assertEquals(COOK_TIME, recipe.cookTime)
        assertEquals(PREP_TIME, recipe.prepTime)
        assertEquals(DESCRIPTION, recipe.description)
        assertEquals(DIRECTIONS, recipe.directions)
        assertEquals(SERVINGS, recipe.servings)
        assertEquals(SOURCE, recipe.source)
        assertEquals(URL, recipe.url)
        assertEquals(SUBMITTED_BY, recipe.submittedBy)
        assertEquals(DIFFICULTY, recipe.difficulty)
        assertEquals(NOTES_ID, recipe.notes!!.id)
        assertEquals(2, recipe.ingredients.size)
        assertEquals(2, recipe.categories.size)
    }

    companion object {
        const val RECIPE_ID = 1.toLong()
        const val COOK_TIME = 20
        const val PREP_TIME = 15
        const val DESCRIPTION = "My Recipe"
        const val DIRECTIONS = "Add a lot of water"
        const val SERVINGS = 5
        const val SOURCE = "Food Lion"
        const val URL = "http://twitter.com"
        const val SUBMITTED_BY = "Alsan de Bomb"
        val DIFFICULTY = Difficulty.EASY

        const val  NOTES_ID = 4.toLong()
        const val CAT_ID1 = 9.toLong()
        const val CAT_ID2 = 8.toLong()
        const val INGRED_ID1 = 6.toLong()
        const val INGRED_ID2 = 7.toLong()
    }
}
