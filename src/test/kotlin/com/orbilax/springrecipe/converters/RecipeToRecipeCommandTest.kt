package com.orbilax.springrecipe.converters

import com.orbilax.springrecipe.model.*
import org.junit.Assert.assertEquals
import org.junit.Test

class RecipeToRecipeCommandTest {

    private val converter = RecipeToRecipeCommand(CategoryToCategoryCommand(),
            IngredientToIngredientCommand(UomToUomCommand()),
            NotesToNotesCommand())

    @Test
    fun convert() {
        //given
        val recipe = Recipe()
        recipe.id = RECIPE_ID
        recipe.cookTime = COOK_TIME
        recipe.prepTime = PREP_TIME
        recipe.description = DESCRIPTION
        recipe.directions = DIRECTIONS
        recipe.servings = SERVINGS
        recipe.source = SOURCE
        recipe.url = URL
        recipe.submittedBy = SUBMITTED_BY
        recipe.difficulty = DIFFICULTY

        val notes = Notes()
        notes.id = NOTES_ID
        recipe.notes = notes

        val category = Category()
        category.id = CAT_ID1
        val category2 = Category()
        category2.id = CAT_ID2

        recipe.addCategory(category)
        recipe.addCategory(category2)

        val ingredient = Ingredient()
        ingredient.id = INGRED_ID1
        val ingredient2 = Ingredient()
        ingredient.id = INGRED_ID2

        recipe.addIngredient(ingredient)
        recipe.addIngredient(ingredient2)

        //when
        val recipeCommand = converter.convert(recipe)
        //then
        assertEquals(RECIPE_ID, recipeCommand.id)
        assertEquals(COOK_TIME, recipeCommand.cookTime)
        assertEquals(PREP_TIME, recipeCommand.prepTime)
        assertEquals(DESCRIPTION, recipeCommand.description)
        assertEquals(DIRECTIONS, recipeCommand.directions)
        assertEquals(SERVINGS, recipeCommand.servings)
        assertEquals(SOURCE, recipeCommand.source)
        assertEquals(URL, recipeCommand.url)
        assertEquals(SUBMITTED_BY, recipeCommand.submittedBy)
        assertEquals(DIFFICULTY, recipeCommand.difficulty)
        assertEquals(NOTES_ID, recipeCommand.notes.id)
        assertEquals(2, recipeCommand.ingredients.size)
        assertEquals(2, recipeCommand.categories.size)
    }

    companion object {
        const val RECIPE_ID = 1.toLong()
        const val COOK_TIME = 20
        const val PREP_TIME = 15
        const val DESCRIPTION = "My Recipe"
        const val DIRECTIONS = "Add a lot of water"
        const val SERVINGS = 5
        const val SOURCE = "Food Lion"
        const val URL = "http://twitter.com"
        const val SUBMITTED_BY = "Alsan de Bomb"
        val DIFFICULTY = Difficulty.EASY

        const val  NOTES_ID = 4.toLong()
        const val CAT_ID1 = 9.toLong()
        const val CAT_ID2 = 8.toLong()
        const val INGRED_ID1 = 6.toLong()
        const val INGRED_ID2 = 7.toLong()
    }
}