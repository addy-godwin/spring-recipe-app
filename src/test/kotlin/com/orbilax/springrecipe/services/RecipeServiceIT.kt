package com.orbilax.springrecipe.services

import com.orbilax.springrecipe.converters.RecipeCommandToRecipe
import com.orbilax.springrecipe.converters.RecipeToRecipeCommand
import com.orbilax.springrecipe.repository.RecipeRepository
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest
class RecipeServiceIT {

    @Autowired
    lateinit var recipeService: RecipeService

    @Autowired
    lateinit var recipeRepository: RecipeRepository

    @Autowired
    lateinit var recipeCommandToRecipe: RecipeCommandToRecipe

    @Autowired
    lateinit var recipeToRecipeCommand: RecipeToRecipeCommand

    @Transactional
    @Test
    fun testSaveOfDescription() {
        //given
        val recipes = recipeRepository.findAll()
        val recipe = recipes.iterator().next()
        val recipeCommand = recipeToRecipeCommand.convert(recipe)

        //when
        recipeCommand.description  = NEW_DESCRIPTION
        val savedRecipeCommand = recipeService.saveRecipeCommand(recipeCommand)

        //then
        assertEquals(NEW_DESCRIPTION, savedRecipeCommand.description)
        assertEquals(recipeCommand.id, savedRecipeCommand.id)
        assertEquals(recipeCommand.categories.size, savedRecipeCommand.categories.size)
        assertEquals(recipeCommand.ingredients.size, savedRecipeCommand.ingredients.size)
    }

    companion object {
        const val NEW_DESCRIPTION = "New Description"
    }
}