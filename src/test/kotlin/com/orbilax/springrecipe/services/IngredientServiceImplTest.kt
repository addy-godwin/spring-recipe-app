package com.orbilax.springrecipe.services

import com.nhaarman.mockitokotlin2.*
import com.orbilax.springrecipe.commands.IngredientCommand
import com.orbilax.springrecipe.converters.IngredientCommandToIngredient
import com.orbilax.springrecipe.converters.IngredientToIngredientCommand
import com.orbilax.springrecipe.converters.UomCommandToUom
import com.orbilax.springrecipe.converters.UomToUomCommand
import com.orbilax.springrecipe.model.Ingredient
import com.orbilax.springrecipe.model.Recipe
import com.orbilax.springrecipe.repository.RecipeRepository
import com.orbilax.springrecipe.repository.UnitOfMeasureRepository
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.*

class IngredientServiceImplTest {

    @Mock
    private lateinit var recipeRepository: RecipeRepository

    @Mock
    private lateinit var unitOfMeasureRepository: UnitOfMeasureRepository

    private lateinit var ingredientService: IngredientService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        ingredientService = IngredientServiceImpl(recipeRepository, unitOfMeasureRepository,
                IngredientToIngredientCommand(UomToUomCommand()), IngredientCommandToIngredient(UomCommandToUom()))
    }

    @Test
    fun findByRecipeIdAndIngredientId() {
        //given
        val recipe = Recipe(); recipe.id = 1

        val ingredient = Ingredient(); ingredient.id = 2
        val ingredient2 = Ingredient(); ingredient.id = 3
        val ingredient3 = Ingredient(); ingredient.id = 4

        recipe.addIngredient(ingredient)
        recipe.addIngredient(ingredient2)
        recipe.addIngredient(ingredient3)

        //when
        whenever(recipeRepository.findById(anyLong())) doReturn Optional.of(recipe)

        //then
        val ingredientCommand = ingredientService.findByRecipeIdAndIngredientId(1, 4)

        assertEquals(4.toLong(), ingredientCommand.id)
        assertEquals(1.toLong(), ingredientCommand.recipeId)
        verify(recipeRepository, times(1)).findById(anyLong())
    }

    @Test
    fun saveIngredientCommand() {
        //given
        val ingredientCommand = IngredientCommand()
        ingredientCommand.id = 2; ingredientCommand.recipeId = 3

        val recipeOptional = Optional.of(Recipe())

        val savedRecipe = Recipe()
        savedRecipe.addIngredient(Ingredient())
        savedRecipe.ingredients.iterator().next().id = 2

        whenever(recipeRepository.findById(anyLong())) doReturn recipeOptional
        whenever(recipeRepository.save(any<Recipe>())) doReturn savedRecipe

        //when
        val savedCommand = ingredientService.saveIngredientCommand(ingredientCommand)

        //then
        assertEquals(2.toLong(), savedCommand.id)
        verify(recipeRepository, times(1)).findById(anyLong())
        verify(recipeRepository, times(1)).save(any<Recipe>())
    }

    @Test
    fun deleteById() {

    }
}