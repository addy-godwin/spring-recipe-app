package com.orbilax.springrecipe.services

import com.nhaarman.mockitokotlin2.*
import com.orbilax.springrecipe.model.Recipe
import com.orbilax.springrecipe.repository.RecipeRepository
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.mock.web.MockMultipartFile
import java.util.*

class ImageServiceImplTest {

    @Mock
    private lateinit var recipeRepository: RecipeRepository

    private lateinit var imageService: ImageService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        imageService = ImageServiceImpl(recipeRepository)
    }

    @Test
    fun saveImageFile() {
        //given
        val id: Long = 1
        val multipartFile = MockMultipartFile("imagefile", "testing.txt", "text/plain", "Billion Years".toByteArray())

        val recipe = Recipe();  recipe.id = id
        val recipeOptional = Optional.of(recipe)

        whenever(recipeRepository.findById(anyLong())) doReturn recipeOptional

        val argumentCaptor = argumentCaptor<Recipe>()
        //when
        imageService.saveImageFile(id, multipartFile)

        //then
        verify(recipeRepository, times(1)).save(argumentCaptor.capture())
        val savedRecipe = argumentCaptor.firstValue
        assertEquals(multipartFile.bytes.size, savedRecipe.image?.size)
    }
}