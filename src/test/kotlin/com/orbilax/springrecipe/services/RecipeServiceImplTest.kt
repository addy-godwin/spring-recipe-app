package com.orbilax.springrecipe.services

import com.nhaarman.mockitokotlin2.*
import com.orbilax.springrecipe.converters.RecipeCommandToRecipe
import com.orbilax.springrecipe.converters.RecipeToRecipeCommand
import com.orbilax.springrecipe.exception.NotFoundException
import com.orbilax.springrecipe.model.Recipe
import com.orbilax.springrecipe.repository.CategoryRepository
import com.orbilax.springrecipe.repository.RecipeRepository
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.*

class RecipeServiceImplTest {

    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var recipeRepository: RecipeRepository

    @Mock
    private lateinit var categoryRepository: CategoryRepository

    @Mock
    private lateinit var recipeToRecipeCommand: RecipeToRecipeCommand

    @Mock
    private lateinit var recipeCommandToRecipe: RecipeCommandToRecipe

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        recipeService = RecipeServiceImpl(recipeRepository, categoryRepository, recipeCommandToRecipe, recipeToRecipeCommand)
    }

    @Test
    fun getRecipes() {
        val recipe = Recipe()
        val returnedSet = HashSet<Recipe>()
        returnedSet.add(recipe)

        whenever(recipeService.getRecipes())
                .doReturn(returnedSet)

        val recipes = recipeService.getRecipes()

        assertEquals(recipes.size, 1)
        verify(recipeRepository, times(1)).findAll()
    }

    @Test
    fun findRecipeById() {
        val recipe = Recipe(); recipe.id = 1
        val recipeOptional = Optional.of(recipe)

        whenever(recipeRepository.findById(anyLong())).thenReturn(recipeOptional)

        val recipeReturned = recipeService.findRecipeById(1.toLong())
        assertNotNull("Null recipe returned", recipeReturned)
        verify(recipeRepository, times(1)).findById(anyLong())
        verify(recipeRepository, never()).findAll()
    }

    @Test(expected = NotFoundException::class)
    fun getRecipeByIdNotFound() {
        //given
        val recipeOptional = Optional.empty<Recipe>()
        whenever(recipeRepository.findById(anyLong())) doReturn recipeOptional

        val recipeReturned = recipeService.findRecipeById(1)
        //should throw
    }

    @Test
    fun deleteById() {
        //given
        val idToDelete = 2.toLong()

        //when
        recipeService.deleteById(idToDelete)

        //no when since method returns Unit

        //then
        verify(recipeRepository, times(1)).deleteById(anyLong())
    }
}