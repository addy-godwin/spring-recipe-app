package com.orbilax.springrecipe.services

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.orbilax.springrecipe.converters.UomToUomCommand
import com.orbilax.springrecipe.model.UnitOfMeasure
import com.orbilax.springrecipe.repository.UnitOfMeasureRepository
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class UnitOfMeasureServiceImplTest {

    @Mock
    private lateinit var unitOfMeasureRepository: UnitOfMeasureRepository

    private lateinit var uomToUomCommand: UomToUomCommand
    private lateinit var uomService: UnitOfMeasureService
    @Before
    fun setUp() {

        MockitoAnnotations.initMocks(this)

        uomToUomCommand = UomToUomCommand()

        uomService = UnitOfMeasureServiceImpl(unitOfMeasureRepository, uomToUomCommand)
    }

    @Test
    fun listAllUoms() {

       //given
        val unitOfMeasures: MutableSet<UnitOfMeasure> = HashSet()
        val uom1 = UnitOfMeasure(); uom1.id = 1
        unitOfMeasures.add(uom1)

        val uom2 = UnitOfMeasure(); uom2.id = 2
        unitOfMeasures.add(uom2)

        whenever(unitOfMeasureRepository.findAll()) doReturn unitOfMeasures

        //when
        val commands = uomService.listAllUoms()

        //then
        assertEquals(2, commands.size)
        verify(unitOfMeasureRepository, times(1)).findAll()
    }
}