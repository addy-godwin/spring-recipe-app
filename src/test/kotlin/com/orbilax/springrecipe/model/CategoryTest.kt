package com.orbilax.springrecipe.model

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CategoryTest {

    private lateinit var category: Category

    @Before
    fun setUp() {
        category = Category()
    }

    @Test
    fun getId() {
        val idValue = 4.toLong()
        category.id = idValue

        assertEquals(idValue, category.id)
    }

    @Test
    fun getDescription() {
    }

    @Test
    fun getRecipes() {
    }
}