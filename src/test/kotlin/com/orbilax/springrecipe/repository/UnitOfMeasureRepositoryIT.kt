package com.orbilax.springrecipe.repository

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@DataJpaTest
class UnitOfMeasureRepositoryIT {

    @Autowired
    lateinit var unitOfMeasureRepository: UnitOfMeasureRepository

    @Before
    fun setUp() {

    }

    @Test
    fun findByDescription() {
        val uomString = "Cup"
        val uomOptional = unitOfMeasureRepository.findByDescription(uomString)

        assertEquals(uomString, uomOptional.get().description)
    }
}