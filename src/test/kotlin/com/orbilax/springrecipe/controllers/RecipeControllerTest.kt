package com.orbilax.springrecipe.controllers

import com.nhaarman.mockitokotlin2.*
import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.exception.NotFoundException
import com.orbilax.springrecipe.model.Recipe
import com.orbilax.springrecipe.services.RecipeService
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.ui.Model
import java.util.*

class RecipeControllerTest {

    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var model: Model

    private lateinit var recipeController: RecipeController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        recipeController = RecipeController(recipeService)
        mockMvc = MockMvcBuilders.standaloneSetup(recipeController)
                .setControllerAdvice(ControllerExceptionHandler()).build()
    }

    @Test
    fun getRecipes() {
        //given
        val recipeSet = HashSet<Recipe>()
        recipeSet.add(Recipe())
        recipeSet.add(Recipe())

        whenever(recipeService.getRecipes()).doReturn(recipeSet)

        //when
        val viewName = recipeController.getRecipes(model)

        //then
        assertEquals("recipe/all", viewName)
        verify(recipeService, times(1)).getRecipes()

        argumentCaptor<Set<Recipe>>().apply {
            verify(model, times(1)).addAttribute(eq("recipes"), capture())
            val setInController = firstValue
            assertEquals(2, setInController.size)
        }
    }

    @Test
    fun getRecipesMockMVC() {
        mockMvc.perform(get("/recipe/all"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/all"))
    }

    @Test
    fun getRecipeByIdMockMVC() {
        val recipe = Recipe(); recipe.id = 1

        whenever(recipeService.findRecipeById(anyLong())).doReturn(recipe)

        mockMvc.perform(get("/recipe/1/show"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/show"))
                .andExpect(model().attributeExists("recipe"))
    }

    @Test
    fun testGetRecipeNotFound() {
        val recipe = Recipe(); recipe.id = 1

        whenever(recipeService.findRecipeById(anyLong())) doThrow NotFoundException::class

        mockMvc.perform(get("/recipe/1/show"))
                .andExpect(status().isNotFound)
                .andExpect(view().name("404error"))
    }

    @Test
    fun testRecipeNumberFormatException() {
        mockMvc.perform(get("/recipe/asdf/show"))
                .andExpect(status().isBadRequest)
                .andExpect(view().name("400error"))
    }

    @Test
    fun submitRecipeTest() {

        mockMvc.perform(get("/recipe/new"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/recipeform"))
                .andExpect(model().attributeExists("recipe"))
    }

    @Test
    fun updateRecipeTest() {
        //given
        val recipeCommand = RecipeCommand(); recipeCommand.id = 4

        //when
        whenever(recipeService.findCommandById(anyLong())).doReturn(recipeCommand)

        //then
        mockMvc.perform(get("/recipe/1/update"))
                .andExpect(status().isOk)
                .andExpect(model().attributeExists("recipe"))
                .andExpect(view().name("recipe/recipeform"))
    }

    @Test
    fun saveOrUpdateRecipeTest() {
        //given
        val recipeCommand = RecipeCommand(); recipeCommand.id = 2

        //when
        whenever(recipeService.saveRecipeCommand(any())) doReturn recipeCommand

        //then
        mockMvc.perform(post("/recipe/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "5")
                .param("description", "some string")
                .param("directions", "i love it")
        )
                .andExpect(status().is3xxRedirection)
                .andExpect(view().name("redirect:/recipe/2/show"))
    }

    @Test
    fun testPostNewRecipeFormValidationFail() {
        //given
        val recipeCommand = RecipeCommand(); recipeCommand.id = 2

        //when
        whenever(recipeService.saveRecipeCommand(any())) doReturn recipeCommand

        //then
        mockMvc.perform(post("/recipe/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "5")
                .param("description", "some string")
        )
                .andExpect(status().isOk)
                .andExpect(model().attributeExists("recipe"))
                .andExpect(view().name("recipe/recipeform"))
    }

    @Test
    fun deleteRecipeActionTest() {
        mockMvc.perform(get("/recipe/1/delete"))
                .andExpect(status().is3xxRedirection)
                .andExpect(view().name("redirect:/recipe/all"))

        verify(recipeService, times(1)).deleteById(anyLong())
    }
}