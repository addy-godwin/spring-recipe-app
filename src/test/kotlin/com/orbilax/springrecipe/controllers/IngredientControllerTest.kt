package com.orbilax.springrecipe.controllers

import com.nhaarman.mockitokotlin2.*
import com.orbilax.springrecipe.commands.IngredientCommand
import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.commands.UnitOfMeasureCommand
import com.orbilax.springrecipe.services.IngredientService
import com.orbilax.springrecipe.services.RecipeService
import com.orbilax.springrecipe.services.UnitOfMeasureService
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

class IngredientControllerTest {

    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var ingredientService: IngredientService

    @Mock
    private lateinit var unitOfMeasureService: UnitOfMeasureService

    private lateinit var ingredientController: IngredientController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        ingredientController = IngredientController(recipeService, ingredientService, unitOfMeasureService)
        mockMvc = MockMvcBuilders.standaloneSetup(ingredientController).build()
    }

    @Test
    fun testListIngredients() {
        //given
        val recipeCommand = RecipeCommand()
        whenever(recipeService.findCommandById(anyLong())) doReturn recipeCommand

        //when
        mockMvc.perform(get("/recipe/1/ingredients"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/ingredient/list"))
                .andExpect(model().attributeExists("recipe"))

        //then
        verify(recipeService, times(1)).findCommandById(anyLong())
    }

    @Test
    fun testShowIngredient() {
        //given
        val ingredientCommand = IngredientCommand()

        //when
        whenever(ingredientService.findByRecipeIdAndIngredientId(anyLong(), anyLong())) doReturn ingredientCommand

        //then
        mockMvc.perform(get("/recipe/1/ingredient/2/show"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/ingredient/show"))
                .andExpect(model().attributeExists("ingredient"))
    }

    @Test
    fun testNewIngredient() {

        //given
        val recipeCommand = RecipeCommand(); recipeCommand.id = 1

        //when
        whenever(recipeService.findCommandById(anyLong())) doReturn recipeCommand
        whenever(unitOfMeasureService.listAllUoms()) doReturn HashSet()

        //then
        mockMvc.perform(get("/recipe/1/ingredient/new"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/ingredient/ingredientform"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"))

        verify(recipeService, times(1)).findCommandById(anyLong())
    }

    @Test
    fun updateIngredientForm() {

        //given
        val ingredientCommand = IngredientCommand()
        ingredientCommand.id = 1; ingredientCommand.recipeId = 2

        val uom1 = UnitOfMeasureCommand(); uom1.id = 1
        val uom2 = UnitOfMeasureCommand(); uom2.id = 2
        val uomList = mutableSetOf(uom1, uom2)

        //when
        whenever(ingredientService.findByRecipeIdAndIngredientId(anyLong(), anyLong())) doReturn ingredientCommand
        whenever(unitOfMeasureService.listAllUoms()) doReturn uomList

        mockMvc.perform(get("/recipe/1/ingredient/2/update"))
                .andExpect(status().isOk)
                .andExpect(view().name("recipe/ingredient/ingredientform"))
                .andExpect(model().attributeExists("ingredient"))
                .andExpect(model().attributeExists("uomList"))
    }

    @Test
    fun testSaveOrUpdate() {
        //given
        val command = IngredientCommand()
        command.id = 2; command.recipeId = 3

        //when
        whenever(ingredientService.saveIngredientCommand(any())) doReturn command

        //then
        mockMvc.perform(post("/recipe/3/ingredient")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "3")
                .param("description", "some string"))
                .andExpect(status().is3xxRedirection)
                .andExpect(view().name("redirect:/recipe/3/ingredient/2/show"))
}

    @Test
    fun deleteIngredientTest() {

        mockMvc.perform( get("/recipe/1/ingredient/2/delete"))
                .andExpect(status().is3xxRedirection)
                .andExpect(view().name("redirect:/recipe/1/ingredients"))

        verify(ingredientService, times(1)).deleteById(anyLong(), anyLong())
    }
}