package com.orbilax.springrecipe.controllers

import com.nhaarman.mockitokotlin2.*
import com.orbilax.springrecipe.commands.RecipeCommand
import com.orbilax.springrecipe.services.ImageService
import com.orbilax.springrecipe.services.RecipeService
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

class ImageControllerTest {

    @Mock
    private lateinit var imageService: ImageService

    @Mock
    private lateinit var recipeService: RecipeService

    private lateinit var imageController: ImageController

    private lateinit var mockMvc: MockMvc

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        imageController = ImageController(imageService, recipeService)
        mockMvc = MockMvcBuilders.standaloneSetup(imageController).build()
    }

    @Test
    fun testFormGet() {
        //given
        val recipeCommand = RecipeCommand(); recipeCommand.id = 1

        whenever(recipeService.findCommandById(anyLong())) doReturn recipeCommand

        //when
        mockMvc.perform(get("/recipe/1/image"))
                .andExpect(status().isOk)
                .andExpect(model().attributeExists("recipe"))
                .andExpect(view().name("recipe/imageuploadform"))
        //then
        verify(recipeService, times(1)).findCommandById(anyLong())
    }

    @Test
    fun testImagePost() {
        //given
        val multipartFile = MockMultipartFile("imagefile", "testing.txt", "text/plain", "Welcome to america".toByteArray())
        //when
        mockMvc.perform(multipart("/recipe/1/image").file(multipartFile))
                .andExpect(status().is3xxRedirection)
                .andExpect(header().string("Location", "/recipe/1/show"))
        //then
        verify(imageService, times(1)).saveImageFile(anyLong(), any())
    }

    @Test
    fun renderFromDB() {
        //given
        val recipeCommand = RecipeCommand(); recipeCommand.id = 1
        val s = "fake image text"

        recipeCommand.image  = s.toByteArray()

        whenever(recipeService.findCommandById(anyLong())) doReturn  recipeCommand

        //when
        val response = mockMvc.perform(get("/recipe/1/recipeimage"))
                .andExpect(status().isOk)
                .andReturn().response

        val responseBytes = response.contentAsByteArray

        assertEquals(s.toByteArray().size, responseBytes.size)
    }
}